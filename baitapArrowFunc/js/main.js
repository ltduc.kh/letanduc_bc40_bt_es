const colorList = [
    "pallet", 
    "viridian", 
    "pewter", 
    "cerulean", 
    "vermillion", 
    "lavender", 
    "celadon", 
    "saffron", 
    "fuschia", 
    "cinnabar"
];
let container = document.getElementById("colorContainer");
loadColorPick = (()=>{
    for (let index = 0; index < colorList.length; index++)
        container.innerHTML += 0 == index ? "<button class='color-button " + colorList[index] + " active'></button>" : "<button class='color-button " + colorList[index] + "'></button>"
}
),
loadColorPick();
let colorPicker = document.getElementsByClassName("color-button")
  , house = document.getElementById("house");
for (let index = 0; index < colorPicker.length; index++)
    colorPicker[index].addEventListener("click", function() {
        changeColor(colorList[index], index)
    });
changeColor = ((index,viTri)=>{
    for (let index = 0; index < colorPicker.length; index++)
        colorPicker[index].classList.remove("active");
    colorPicker[viTri].classList.add("active"),
    house.className = "house " + index
}
);




